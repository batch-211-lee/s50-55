// import {Fragment} from "react";
import {Container} from "react-bootstrap";
import {BrowserRouter as Router, Routes, Route} from "react-router-dom";
import AppNavbar from "./components/AppNavbar";
import Home from "./pages/Home";
import Courses from "./pages/Courses";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Error from "./pages/Error";
import {useState} from 'react';
import {UserProvider} from "./UserContext"
import './App.css';

function App() {

  //This will be used to store user information and will be used for validating if a user is logged in on the app or not.
  const [user,setUser] = useState({
    email: localStorage.getItem("email")
  })

  //Function for clearing localStorage when logging out
  const unsetUser=()=>{
    localStorage.clear()
  };

  return (

    <UserProvider value = {{user,setUser, unsetUser}}>
        <Router>
          <AppNavbar/>
            <Container>
              <Routes>
                <Route path="/" element={<Home/>}/>
                <Route path="/courses" element={<Courses/>}/>
                <Route path="/login" element={<Login/>}/>
                <Route path="/register" element={<Register/>}/>
                <Route path = "/logout" element={<Logout/>}/>
                <Route path = "*" element={<Error/>}/>
              </Routes>
            </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
