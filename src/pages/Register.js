// import Button from 'react-bootstrap/Button';
// import Form from 'react-bootstrap/Form';

import {Form, Button} from "react-bootstrap";
import {useState,useEffect, useContext} from "react";
import UserContext from "../UserContext";
import {Navigate} from "react-router-dom";

//Define state hooks for all input fields and an "isActive" state for conditional rendering of the submit btn

export default function Register() {
  const [firstName, setFirstName] =useState("");
  const [lastName, setLastName] =useState("");
  const [email, setEmail] =useState("");
  const [mobileNo, setMobileNo] =useState("");
  const [password1, setPassword1] =useState("");
  const [password2, setPassword2] =useState("");

  //Creat a state to determine whether the submit button is enable or not

  const [isActive, setIsActive] =useState("");
  const {user, setUser} = useContext(UserContext);

console.log(firstName);
console.log(lastName);
console.log(email);
console.log(mobileNo);
console.log(password1);
console.log(password2);

function registerUser(e){

  e.preventDefault();
  
  setFirstName("");
  setLastName("");
  setEmail("");
  setMobileNo("");
  setPassword1("");
  setPassword2("");

  alert("thank you for registering")
}
  /*
      Two Way Binding
      -it is done so that we can assure that we can save the input into our states as we type into the input elements. This is done so that we don't have to save it just before we submit

      e.target - current element where event happens
      e.target.value - current value of the element wher event happened
  */

useEffect(()=>{
  if((firstName !=="" && lastName !== "" && mobileNo.length === 11 && email !== "" && password1 !== "" && password2 !=="")&&(password1===password2)){
    setIsActive(true)
  }else{
    setIsActive(false)
  }
},[firstName,lastName,email,mobileNo,password1,password2])

  return (
      (user.email !== null)
    ?
    <Navigate to="/courses"/>
    :
    <Form onSubmit={(e)=>registerUser(e)}>
      <Form.Group className="mb-3" controlId="firstName">
        <Form.Label>First Name:</Form.Label>
        <Form.Control type="text"
        placeholder="First Name"
        value={firstName}
        onChange={e=>setFirstName(e.target.value)}        
         required />
      </Form.Group>

      <Form.Group className="mb-3" controlId="lastName">
        <Form.Label>Last Name</Form.Label>
        <Form.Control type="text" placeholder="Last Name"
        value={lastName}
        onChange={e=>setLastName(e.target.value)}
         required />
      </Form.Group>

      <Form.Group className="mb-3" controlId="userEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control type="email" placeholder="Enter email" value={email}
        onChange={e=>setEmail(e.target.value)}
        required />
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="mobileNo">
        <Form.Label>Mobile No.</Form.Label>
        <Form.Control type="text" placeholder="+63xxx xxx xxxx"
        value={mobileNo}
        onChange={e=>setMobileNo(e.target.value)}
         required />
      </Form.Group>

      <Form.Group className="mb-3" controlId="password1">
        <Form.Label>Password</Form.Label>
        <Form.Control type="password" placeholder="Password"
        value={password1}
        onChange={e=>setPassword1(e.target.value)}
         required />
      </Form.Group>

      <Form.Group className="mb-3" controlId="password2">
        <Form.Label>Password</Form.Label>
        <Form.Control type="password" placeholder="Verify Password"
        value={password2}
        onChange={e=>setPassword2(e.target.value)}
          />
      </Form.Group>

      {isActive ?
      <Button variant="success" type="submit" id="submitBtn">
        Submit
      </Button>
      :
      <Button variant="danger" type="submit" id="submitBtn" disabled>
        Submit
      </Button>
      }
    </Form>
  );
};

