import {Form, Button} from "react-bootstrap";
import {useState,useEffect, useContext} from "react";
import UserContext from "../UserContext";
import {Navigate} from "react-router-dom";

export default function Login() {
	const [email, setEmail] =useState("");
	const [password, setPassword] =useState("");
	const [isActive, setIsActive] = useState("");

	//Allows us to consume the User Context/Data and its properties for validation
	const {user, setUser} = useContext(UserContext);

	function loginUser (e){
		// e.preventDefault();

		//Set the email of the authenticated user in the local storage
		/*
			Syntax:
				localStorage.setItem("propertyName", value)
		*/
		localStorage.setItem("email",email);

		//Set the global user state to have properties obtained from local storage.
		//This will pass the data to the UserContext which is ready to use from all different endpoints/pages
		setUser({
			email: localStorage.getItem("email")
		});

		setEmail("");
		setPassword("");
		alert ("You are now logged in")
	};

	useEffect(()=>{
		if(email !== "" && password !== ""){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [email,password])

	return (

		//Conditional rendering 
		//Logic - if there is a user logged-in in the web application, endpoint "/login" or "/register" should not be accessible. The should be navigated to courses tab instead.
		(user.email !== null)
		?
		<Navigate to="/courses"/>
		//Else - if the localStorage is empty, the user is allowed to access the login page.
		:
		<Form onSubmit={(e)=>loginUser(e)}>
			<h1>Login</h1>
			  	<Form.Group className="mb-3" controlId="email">
        			<Form.Label>Email Address</Form.Label>
        			<Form.Control type="email" placeholder="Enter email"
        				value={email}
        				onChange={e=>setEmail(e.target.value)}
         				required />
      			</Form.Group>

      			<Form.Group className="mb-3" controlId="password">
        			<Form.Label>Password</Form.Label>
        			<Form.Control type="password" placeholder="Password"
        			value={password}
        			onChange={e=>setPassword(e.target.value)}
        			  />
      			</Form.Group>

      			{isActive ?
      				<Button variant="primary" type="submit" id="submitBtn">
       					 Login
      				</Button>
      				:
      				<Button variant="danger" type="submit" id="submitBtn" disabled>
        				Login
      				</Button>
      			}
    		</Form>
		)
}