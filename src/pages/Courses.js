import coursesData from "../data/coursesData"
import CourseCard from "../components/CourseCard";

export default function Courses(){

	//using the map array method, we can loop through our courseData and dynamically render any number of CourseCards depending on how many array elements are present in our data

	//props are a way to pass valid JS data from parent component to child component. You can pass any prop even functions.
	//map returns an array, which we can display in the page via the component function's return statement
	const courses = coursesData.map((course)=>{
		return <CourseCard courseProp={course} key={course.id}/>
	})

	return (
		<>
			{courses}
		</>
	)
}