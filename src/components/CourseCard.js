import {useState, useEffect} from "react"
import {Col,Card, Button} from "react-bootstrap";

export default function Coursecard({courseProp}){

	//useState hook:
	//A hook in React is a kind of tool, the useState hook allows creation and manipulation of states

	//States are a way for React to keep track of any value and associate it with a component

	//When a state changes, React re-renders ONLY the specific component or past of the component that change

	//State is a special kind of variable

	//array deconstructing to get the state and the setter
	const [count, setCount] = useState(0);
	const [seat, setSeat] = useState(10);

	//Syntax:
	//	const [state, setState] = useState(default state)


	let {name,description,price} = courseProp;

	//refractor enroll function and instead use useEffect hooks
	function enroll(){
		if(count !==10){
			setSeat(seat -1);
			setCount(count + 1);
		}
	}

	//Apply the useEffect hook
	//useEffect makes any given code block happen when a state changes AND when a component first mounts (such as on initial page load)

	/*
		Syntax:
			useEffect(function, [dependencies])
	*/

	useEffect(()=>{
		if(seat ===0){
			alert("no more seats available")
		}
	},[count,seat])

	//alert("No more seats available")

	return (
		<Card>
		      <Card.Body>
		        <Card.Title>{name}</Card.Title>
		        	<Card.Subtitle>Description:</Card.Subtitle> 
		        	<Card.Text>{description}</Card.Text>
		        	<Card.Subtitle>Price:</Card.Subtitle>
		        	<Card.Text>PHP {price}</Card.Text>
		        	<Card.Text>Enrollees: {count}</Card.Text>
		        	<Card.Text>Seats: {seat}</Card.Text>
		      <Button variant="primary" onClick={enroll}>Enroll now!</Button>
		      </Card.Body>
		    </Card>
	)
}