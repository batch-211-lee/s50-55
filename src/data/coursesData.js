const coursesData = [
	{
		id:"wdc001",
		name: "PHP-Laravel",
		description:"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod",
		price: 4500,
		onOffer: true
	},
	{
		id:"wdc002",
		name: "Python - Django",
		description:"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod",
		price:50000,
		onOffer:true
	},
	{
		id:"wdc003",
		name: "Java- Srpingboot",
		description:"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod",
		price:5500,
		onOffer: true
	},
		{
		id:"wdc004",
		name: "C#",
		description:"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod",
		price:5500,
		onOffer: true
	}
];

export default coursesData;